
A Shiny application based on R, illustrating how the climate projections are constrained by observations using the Kriging for Climate Change (KCC) package. The application is running at <a href="https://saidqasmi.shinyapps.io/KCC-shinyapp/">https://saidqasmi.shinyapps.io/KCC-shinyapp/</a>, but can be installed locally by following the instructions bellow.

Content
-------

* [Requirements](#requirements)
* [Related Content](#related-content)
* [Contributors](#contributors)
* [License](#license)


Requirements
------------

- Clone the repository:

<code>git clone git@gitlab.com:saidqasmi/KCC-shinyapp.git</code>

- Install the KCC R package 

Follow the instructions available at: <a href="https://gitlab.com/saidqasmi/KCC">https://gitlab.com/saidqasmi/KCC</a> 

- If necessary, install the package shiny:

<code>install.packages("shiny")</code>


Related Content
---------------

Qasmi, S. and Ribes, A.: Reducing uncertainty in local temperature projections _Science Advances_ DOI:  <a href="https://www.science.org/doi/10.1126/sciadv.abo6872">10.1126/sciadv.abo6872</a>

Ribes, A. et al. (2021): Making climate projections conditional on historical observations. _Science Advances_, 7, eabc0671, DOI: <a href="https://doi.org/10.1126/sciadv.abc0671">10.1126/sciadv.abc0671</a>


Contributors
------------

- Saïd Qasmi (said.qasmi@meteo.fr)
- Aurélien Ribes (aurelien.ribes@meteo.fr)

License
-------

KCC is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

